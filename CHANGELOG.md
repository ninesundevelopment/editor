# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.1](https://bitbucket.org/ninesundevelopment/editor/compare/v0.1.0...v0.1.1) (2019-06-19)



# 0.1.0 (2019-06-18)


### Features

* initial commit ([16d751c](https://bitbucket.org/ninesundevelopment/editor/commits/16d751c))
