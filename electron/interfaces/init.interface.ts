import App = Electron.App;


export interface Init {
  onInit(app: App);
}
