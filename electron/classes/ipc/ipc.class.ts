const BrowserWindow = require('electron').BrowserWindow;
const ipcMain = require('electron').ipcMain;


export class Ipc {
  public static listen(channel, handler: (...any) => any) {
    ipcMain.on(channel, (event, data) => {
      const result = handler(event, data);
      if (result) {
        BrowserWindow
          .getAllWindows()
          .map(bw => {
            bw.webContents.send(channel + '--RESPONSE', result);
          });
      }
    });
  }
}
