"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
var project_controller_1 = require("./controller/project/project.controller");
var Bootstrap = /** @class */ (function () {
    function Bootstrap(app, controller) {
        this.app = app;
        this.controller = controller;
        this.initialize();
    }
    Bootstrap.prototype.initialize = function () {
        var _this = this;
        this.app.on('ready', function () {
            _this.controller.map(function (c) {
                if (typeof c.onInit == 'function') {
                    c.onInit(electron_1.app);
                }
            });
            _this.ready();
        });
    };
    Bootstrap.prototype.ready = function () {
        var _this = this;
        this.win = new electron_1.BrowserWindow({
            height: 400, width: 400,
            webPreferences: {
                nodeIntegration: true,
            },
        });
        this.win.loadURL(url.format({
            pathname: path.join(__dirname, "/../../dist/index.html"),
            protocol: 'file:',
            slashes: true,
        })).then(function () { return _this.win.webContents.openDevTools(); });
    };
    return Bootstrap;
}());
new Bootstrap(electron_1.app, [
    new project_controller_1.ProjectController()
]);
//# sourceMappingURL=main.js.map