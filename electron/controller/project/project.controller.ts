import * as path from 'path';
import {Ipc} from '../../classes/ipc/ipc.class';
import {Init} from '../../interfaces/init.interface';
import App = Electron.App;


export class ProjectController implements Init {

  private workspace: string;

  constructor() {
  }

  public onInit(app: App) {
    this.workspace = path.join(app.getPath('home'), 'antron');
    Ipc.listen('loadWorkspace', this.scanWorkSpace.bind(this));
  }

  scanWorkSpace() {
    return {data: this.workspace};
  }
}
