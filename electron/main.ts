import {app, BrowserWindow} from 'electron';
import * as path from 'path';
import * as url from 'url';
import {ProjectController} from './controller/project/project.controller';
import App = Electron.App;


class Bootstrap {
  private app: App;
  private win: BrowserWindow;
  private controller: Array<any>;

  constructor(app: App, controller: any[]) {
    this.app = app;
    this.controller = controller;
    this.initialize();
  }

  private initialize() {
    this.app.on('ready', () => {
      this.controller.map(c => {
        if (typeof c.onInit == 'function') {
          c.onInit(app);
        }
      });
      this.ready();
    });
  }

  private ready() {
    this.win = new BrowserWindow({
      height: 400, width: 400,
      webPreferences: {
        nodeIntegration: true,
      },
    });

    this.win.loadURL(
      url.format({
        pathname: path.join(__dirname, `/../../dist/index.html`),
        protocol: 'file:',
        slashes: true,
      }),
    ).then(() => this.win.webContents.openDevTools());
  }
}
new Bootstrap(app, [
  new ProjectController()
]);
