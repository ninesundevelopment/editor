"use babel";

const kill = require('tree-kill');
const gulp = require('gulp');
const childProcess = require('child_process');
let electron, build;

gulp.task('build', (done) => {
  let error = false;

  if (build) {
    kill(build.pid);
  }

  build = childProcess.spawn('npm', ['run', 'build']);

  build.stdout.on('data', (data) => console.log(data.toString()));
  build.stderr.on('data', (data) => console.error(data.toString()));

  return build;
});

gulp.task('electron', (done) => {
  if (electron) {
    kill(electron.pid);
  }
  electron = childProcess.spawn('npm', ['run', 'electron']);

  electron.stderr.on('data', (data) => console.error(data.toString()));
  electron.stdout.on('data', (data) => console.log(data.toString()));

  done();
});

gulp.task('default', gulp.series(['build', 'electron', (done) => {
  gulp.watch(['./src/**', '!./**/*.spec.ts'], gulp.series(['build', 'electron']));
  gulp.watch(['./electron/**', '!./electron/dist/**'], gulp.series(['electron']));
  done();
}]));
