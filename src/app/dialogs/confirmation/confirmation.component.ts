import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      closeX: boolean,
      title?: string,
      message: string,
      cancel: () => void,
      confirm?: () => void,
    },
  ) {
  }

  ngOnInit() {
  }

  onCancel() {
    this.dialogRef.close();
    if (typeof this.data.cancel === 'function') {
      this.data.cancel();
    }
  }

  onConfirm() {
    this.dialogRef.close();
    if (typeof this.data.confirm === 'function') {
      this.data.confirm();
    }
  }
}
