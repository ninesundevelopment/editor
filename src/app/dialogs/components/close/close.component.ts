import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';


@Component({
  selector: 'close',
  templateUrl: './close.component.html',
  styleUrls: ['./close.component.scss'],
})
export class CloseComponent implements OnInit {

  @Input() dialogRef: MatDialogRef<any>;

  constructor() {
  }

  ngOnInit() {
  }

  public onClick() {
    this.dialogRef.close();
  }

}
