import {MatDialogConfig} from '@angular/material';


export const DefaultDialogConfig: MatDialogConfig = {
  width: '90%',
  height: '90%',
  disableClose: true,
};

export const ConfirmationDialogConfig: MatDialogConfig = {
  ...DefaultDialogConfig,
  width: '500px',
  maxWidth: '90%',
  height: 'auto',
};

export const NoticeDialogConfig: MatDialogConfig = {
  ...DefaultDialogConfig,
  width: '500px',
  maxWidth: '90%',
  height: 'auto',
  hasBackdrop: false,
  position: {
    top: '20px',
    right: '20px',
  },
};

