import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

export enum NoticeType {
  Success = 0,
  Info    = 1,
  Warning = 2,
  Error   = 3,
}

@Component({
  selector: 'notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
})
export class NoticeComponent implements OnInit {

  public NoticeType = NoticeType;

  public ttl              = 5000;
  public type: NoticeType = NoticeType.Info;
  public current          = 0;

  private pauseCountdown = false;

  constructor(
    public dialogRef: MatDialogRef<NoticeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      ttl: number,
      type: NoticeType,
      message: string,
    },
  ) {

    if (data.ttl !== undefined) {
      this.ttl = data.ttl;
    }

    if (data.type !== undefined) {
      this.type = data.type;
    }

    window.setTimeout(this.reduceTTl.bind(this), 50);
  }

  ngOnInit() {
  }

  onMouseEnter() {
    this.pauseCountdown = true;
  }

  onMouseLeave() {
    this.pauseCountdown = false;
  }

  reduceTTl() {
    if (!this.pauseCountdown) {
      this.current += 50;
    }

    if (this.current >= this.ttl) {
      this.dialogRef.close();
    }

    window.setTimeout(this.reduceTTl.bind(this), 50);
  }
}
