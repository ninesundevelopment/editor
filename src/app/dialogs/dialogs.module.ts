import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CloseComponent} from '@app/dialogs/components/close/close.component';
import {SharedModule} from '@app/shared/shared.module';
import {DialogBodyComponent} from './components/dialog-body/dialog-body.component';
import {DialogFooterComponent} from './components/dialog-footer/dialog-footer.component';
import {DialogHeaderComponent} from './components/dialog-header/dialog-header.component';
import {DialogWrapperComponent} from './components/dialog-wrapper/dialog-wrapper.component';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {NoticeComponent} from './notice/notice.component';


@NgModule({
  declarations: [
    CloseComponent,
    DialogHeaderComponent,
    DialogBodyComponent,
    DialogFooterComponent,
    DialogWrapperComponent,
    ConfirmationComponent,
    NoticeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [
    ConfirmationComponent,
    NoticeComponent,
  ],
})
export class DialogsModule {
}

