import { Component, OnInit } from '@angular/core';
import {ProjectService} from '@app/core/services/project/project.service';

@Component({
  selector: 'bootstrap',
  templateUrl: './bootstrap.component.html',
  styleUrls: ['./bootstrap.component.scss']
})
export class BootstrapComponent implements OnInit {

  constructor(
    private projectService: ProjectService
  ) { }

  ngOnInit() {
  }

  public reload() {
    this.projectService.loadProjects();
  }
}
