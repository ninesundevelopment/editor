import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BootstrapRoutingModule } from './bootstrap-routing.module';
import { BootstrapComponent } from './components/bootstrap/bootstrap.component';

@NgModule({
  declarations: [BootstrapComponent],
  imports: [
    CommonModule,

    BootstrapRoutingModule
  ]
})
export class BootstrapModule { }


