import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {
    path: 'bootstrap',
    loadChildren: '../bootstrap/bootstrap.module#BootstrapModule',
  },
  {
    path: 'editor',
    loadChildren: '../editor/editor.module#EditorModule',
  },
  {
    path: '',
    redirectTo: 'bootstrap',
    pathMatch: 'full',
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    enableTracing: false,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    urlUpdateStrategy: 'eager',
  })],
  exports: [RouterModule],
})
export class CoreRoutingModule {
}
