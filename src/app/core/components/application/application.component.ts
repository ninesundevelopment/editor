import {Component, OnInit} from '@angular/core';
import {ProjectService} from '@app/core/services/project/project.service';


@Component({
  selector: 'application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss'],
})
export class ApplicationComponent implements OnInit {


  constructor(
    private projectService: ProjectService
  ) {
    projectService.loadProjects();
  }

  ngOnInit() {
  }

}
