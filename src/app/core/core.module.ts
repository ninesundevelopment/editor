import {HttpClientModule} from '@angular/common/http';
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DialogsModule} from '@app/dialogs/dialogs.module';
import {SharedModule} from '@app/shared/shared.module';
import {ApplicationComponent} from './components/application/application.component';

import {CoreRoutingModule} from './core-routing.module';


@NgModule({
  declarations: [
    ApplicationComponent,
  ],
  imports: [
    CoreRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    DialogsModule,
  ],
  exports: [],
  providers: [],
  bootstrap: [ApplicationComponent],
})
export class CoreModule {
  /* make sure CoreModule is imported only by one NgModule the AppModule */
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule,
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}


