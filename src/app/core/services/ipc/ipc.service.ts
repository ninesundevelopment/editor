import {Injectable} from '@angular/core';
import {IpcRenderer} from 'electron';
import {Observable} from 'rxjs';
import IpcMessageEvent = Electron.IpcMessageEvent;


export class IpcSuccessResponse {
  public data: any;
}


export class IpcErrorResponse {
  public error: any;
}


@Injectable({
  providedIn: 'root',
})
export class IpcService {
  private ipc: IpcRenderer = window.require('electron').ipcRenderer;

  constructor() {
  }

  public send(channel: string, data: any = null): Observable<any> {
    return new Observable(obs => {
      this.ipc.once(channel + '--RESPONSE',
        (event: IpcMessageEvent, data: any) => {
          if (data.error) {
            obs.error({event, response: data.error});
          } else {
            obs.next({event, response: data.data});
          }
          obs.complete();
        });
      this.ipc.send(channel, data);
    });
  }
}
