import {Injectable} from '@angular/core';
import {IpcService} from '@app/core/services/ipc/ipc.service';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class ProjectService {

  public projects: BehaviorSubject<string[]> = new BehaviorSubject([]);

  constructor(
    private ipcService: IpcService,
  ) {
  }

  public loadProjects() {
    this.ipcService.send('loadWorkspace', {test: false}).subscribe(
      response => {
        this.projects = response;
        console.log(response);
      },
      error => {
        console.error(error);
      },
    );
  }
}




